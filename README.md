# How to run this

Dependencies you need to run all these scripts are listed in `requirements.txt`

You can safely ignore `compute_all_pe.py`.

The interesting script to run simulations is `compute_pe.py`. `simulation.sh` has examples on the usage.

`algos.py` has the cleaned up version of the nonlinear prediction error algorithms. The interesting notebooks
are `aba.ipynb` (it has the earlier, dirtier version of our code) and `lab_report.ipynb`, which is way cleaner but mostly just includes a few plots of the EEG.

On the results folder I've stored the result of the simulations. They are text files, so have a look around. They list the channel, range and parameters before listing the prediction error for both the signal and its surrogates.
