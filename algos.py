import numpy as np
import multiprocessing
import os
from collections import Iterable
from itertools import product
from tqdm import tqdm


def _delayed_signal(signal, m, tau):
    signal_length=signal.shape[0]
    delayed=np.zeros((m,signal_length-(m-1)*tau), dtype=signal.dtype)
    for idx in range(m):
        start = (m-1-idx) * tau
        end = signal_length - idx * tau
        delayed[idx, :] = signal[start:end]
    return delayed


def _euclidean(v1, v2):
    return np.sqrt(np.sum((v1 - v2)**2, axis=0))


def _distance_matrix(signal,w,h):
    rows, columns = signal.shape
    distance = np.ones((columns, columns), dtype=signal.dtype) * np.inf
    for j in range(columns-h):
        i_idxs = np.array(range(j+1+w, columns-h))
        if not len(i_idxs):
            continue
        distances = _euclidean(signal[:, i_idxs], signal[:, j][:, np.newaxis])
        distance[i_idxs, j] = distances
        distance[j, i_idxs] = distance[i_idxs, j]
    return distance


def _prediction_error(signal, dist, m, tau, h, k):
    # dist_sorted_ind returns in each i row the k nearest neighbours 
    # corresponding to point i
    dist_sorted_ind=np.argsort(dist)
    signal_length=signal.shape[0]
    ini=(m-1)*tau
    error_arr=np.zeros((signal_length-ini-h), dtype=signal.dtype)
    # add a restriction due to h timesteps in the distance matrix limits
    i_idxs = np.array(range(ini, signal_length-h))
    error_arr[i_idxs-ini] = (signal[i_idxs+h] -
        signal[dist_sorted_ind[i_idxs-ini,:k]+h+ini].sum(axis=1)/k)**2
    error=np.sqrt(np.mean(error_arr))
    return error, error_arr


def _nonlinear_prediction_error(signal, m, tau, w, h, k):
    """ Calculates the nonlinear prediction error for a given signal
    :param signal: 1D np.array
    :param m: embedding dimension
    :param tau: delay time
    :param w: Theiler correction window
    :param h: horizon window
    :param k: nearest neighbours
    :return: float with the nonlinear prediction error for the given signal and params
    """
    signal=(signal-signal.mean())/signal.std()
    delayed=_delayed_signal(signal, m, tau)
    dist = _distance_matrix(delayed,w,h)
    [error, error_arr]=_prediction_error(signal, dist, m, tau, h, k)
    return error


def _nonlinear_prediction_error_unordered(args):
    surrogate = args[-1]
    args = args[:-1]
    signal_args = args[0]
    signal_slice = signal_args[0][signal_args[1]:signal_args[2]]
    if surrogate:
        signal_slice = phase_randomized_surrogate(signal_slice)
    result = _nonlinear_prediction_error(signal_slice, *args[1:])
    args_return = tuple([signal_args[1], signal_args[2]] + list(args[1:]))
    return args_return, result


def nonlinear_prediction_error(signal, m, tau, w, h, k, sliding_window=None, overlap=0.5,
    surrogate=False, processes=os.cpu_count()):
    """ Calculates the nonlinear prediction error for a given signal
    :param signal: 1D np.array
    :param m: embedding dimension or list of same
    :param tau: delay time or list of same
    :param w: Theiler correction window or list of same
    :param h: horizon window or list of same
    :param k: nearest neighbours or list of same
    :param sliding_window: If specified, calculate the nonlinear prediction error as the mean of
        the sliding window.
    :param overlap: float that specified how much of an overlap between sliding windows there should
        be. Between 0 and 1
    :return: dict with float with the nonlinear prediction error for the given signal and params
             or just a float if there is only 1 combination of params
    """
    if not isinstance(m, Iterable):
        m = [m]
    if not isinstance(tau, Iterable):
        tau = [tau]
    if not isinstance(w, Iterable):
        w = [w]
    if not isinstance(h, Iterable):
        h = [h]
    if not isinstance(k, Iterable):
        k = [k]

    if not sliding_window:
        sliding_window = len(signal)
    
    offset = int(sliding_window * overlap)
    num_cuts = int((len(signal) - offset) / offset)
    signal_cuts = [(signal, idx, idx+sliding_window) for idx in range(0, num_cuts*offset, offset)]
    multiprocessing_args = list(product(signal_cuts, m, tau, w, h, k, [surrogate]))
    args_output = [tuple([args[0][1], args[0][2]]+list(args[1:-1])) for args in multiprocessing_args]
    multiprocessing_results = {args: [] for args in args_output}
    multiprocessing_aggregated_results = {}
    
    with multiprocessing.Pool(processes=processes) as pool:
        with tqdm(total=len(multiprocessing_args)) as pbar:
            for args, result in pool.imap_unordered(
                _nonlinear_prediction_error_unordered, multiprocessing_args
            ):
                pbar.update()
                multiprocessing_results[args].append(result)
        for key, values in multiprocessing_results.items():
            multiprocessing_aggregated_results[key] = np.array(values).mean()
    
    if len(multiprocessing_aggregated_results) == 1:
        return list(multiprocessing_aggregated_results.values())[0]
    else:
        return multiprocessing_aggregated_results


def phase_randomized_surrogate(input_signal):
    """ Calculates a phase randomized surrogate for a given signal
    :param input_signal: 1D np.array
    :return: 1D np.array with same dimensions as input, with phase space randomly shifted
    """
    signal_len = len(input_signal)
    signal_half_len = signal_len // 2
    pre_surrogate = np.fft.fft(input_signal, signal_len)
    rand_phases = np.random.rand(signal_half_len-1) * (2*np.pi)
    pre_surrogate[1:signal_half_len] = pre_surrogate[1:signal_half_len] * np.exp(1j * rand_phases)
    pre_surrogate[signal_half_len+1:] = pre_surrogate[signal_half_len+1:] * np.exp(-1j * rand_phases[::-1])
    output_signal = np.fft.ifft(pre_surrogate, signal_len)
    return output_signal.real
