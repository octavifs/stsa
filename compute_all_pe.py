import argparse
from scipy.io import loadmat
import pandas as pd
from tqdm import tqdm
import numpy as np
from itertools import product

import algos


def main():
    # Beware of sliding window! A higher window means less simulations, but each of them will take more RAM!
    # 4096 means more or less 0.7GB per process, so calculate how much you can fit in your machine according to
    # the number of cores and memory you have available.
    # For the big lab computer (with 32GB of RAM) it is safe to go up to sliding_window=24576
    parser = argparse.ArgumentParser(description='Calculate nonlinear prediction error for EEG channel and surrogates')
    parser.add_argument('eeg', type=str, help="Path where the EEG is stored (.mat)")
    parser.add_argument('start_idx', type=int, help="Start index on the signal")
    parser.add_argument('end_idx', type=int, help="End index on the signal")
    parser.add_argument('output_results_file', type=str, help="File to write results into")
    args = parser.parse_args()

    m = [6]
    tau = [3, 6, 9]
    w = [15, 25, 35]
    h = [55, 65, 75]
    k = [3, 5, 7]
    sliding_window = 4096
    num_surrogates = 9


    # Load the channel data and channel names into a dataframe
    data = loadmat(args.eeg)
    channels = [e[0][0] for e in data["channelNameArray"]]
    df = pd.DataFrame(data["EEG"], columns=channels)
    
    for channel in tqdm(channels):
        print("Calculating PE for channel " + channel)
        channel_signal = df[channel][args.start_idx:args.end_idx].as_matrix()
        surrogates_channel_signal = [algos.phase_randomized_surrogate(channel_signal)
                                     for i in range(num_surrogates)]
        simulation_args = {
            "signal": channel_signal,
            "m": m,
            "tau": tau,
            "w": w,
            "h": h,
            "k": k
        }
        simulation_args_surrogates = [
            {
                "signal": surrogate,
                "m": m,
                "tau": tau,
                "w": w,
                "h": h,
                "k": k
            } for surrogate in surrogates_channel_signal
        ]
        print("Calculating PE for the original signal. This will take a while...")
        pe_channel_signal = algos.nonlinear_prediction_error(*simulation_args.values(),
                                                             sliding_window=sliding_window,
                                                             overlap=0.5)
        print("Calculating PE for the surrogate signals. This will take a while...")
        pe_surrogates_channel_signal = [
            algos.nonlinear_prediction_error(*simulation_args.values(),
                                             sliding_window=sliding_window,
                                             overlap=0.5)
            for simulation_args in tqdm(simulation_args_surrogates)
        ]
        
        with open(args.output_results_file, 'w') as fd:
            for key_args in pe_channel_signal.keys():
                pe_surrogates_channel_signal_key = [pe for pe in pe_surrogates_channel_signal[key_args]]
                results = [
                    "Results for %s, channel %s, from %d to %d" % (args.eeg, args.channel, args.start_idx, args.end_idx),
                    "params: [m, tau, w, h, k] = %s" % (repr(key_args),),
                    "PE original signal: %f" % (pe_channel_signal[key_args],),
                    "PE surrogates: %s" % (repr(pe_surrogates_channel_signal_key),),
                    "PE original - mean(PE surrogates): %f" % (pe_channel_signal - np.mean(pe_surrogates_channel_signal_key),),
                    "",
                ]
                results_str = "\n".join(results)
                print("")
                print(results_str)
                fd.write(results_str)


if __name__ == '__main__':
    main()
