import argparse
from scipy.io import loadmat
import pandas as pd
from tqdm import tqdm
import numpy as np

import algos


def main():
    # Beware of sliding window! A higher window means less simulations, but each of them will take more RAM!
    # 4096 means more or less 0.7GB per process, so calculate how much you can fit in your machine according to
    # the number of cores and memory you have available.
    # For the big lab computer (with 32GB of RAM) it is safe to go up to sliding_window=24576
    parser = argparse.ArgumentParser(description='Calculate nonlinear prediction error for EEG channel and surrogates')
    parser.add_argument('eeg', type=str, help="Path where the EEG is stored (.mat)")
    parser.add_argument('output_results_file', type=str, help="File to write results into")
    parser.add_argument('--channel', dest='channel', type=str, help="Channel you want to analyze")
    parser.add_argument('--m', dest='m', type=int, default=5, action="store", help="Embedding window")
    parser.add_argument('--tau', dest='tau', type=int, default=6, action="store", help="Delay time")
    parser.add_argument('--w', dest='w', type=int, default=25, action="store", help="Theiler correction")
    parser.add_argument('--h', dest='h', type=int, default=65, action="store", help="Horizon window")
    parser.add_argument('--k', dest='k', type=int, default=5, action="store", help="Nearest neighbours")
    parser.add_argument('--sliding-window', dest="sliding_window", type=int, default=4096, action="store", help="Sliding window")
    parser.add_argument('--num-surrogates', dest="num_surrogates", type=int, default=9, action="store", help="Number of surrogates")
    args = parser.parse_args()

    # Load the channel data and channel names into a dataframe
    data = loadmat(args.eeg)
    channels = [e[0][0] for e in data["channelNameArray"]]
    df = pd.DataFrame(data["EEG"], columns=channels)
    
    channels = channels if not args.channel else [args.channel]
    with open(args.output_results_file, 'w') as fd:
        for channel in tqdm(channels):
            channel_signal = df[channel].as_matrix()
            simulation_args = {
                "signal": channel_signal,
                "m": args.m,
                "tau": args.tau,
                "w": args.w,
                "h": args.h,
                "k": args.k
            }
            simulation_args_surrogates_list = [
                {
                    "signal": surrogate,
                    "m": args.m,
                    "tau": args.tau,
                    "w": args.w,
                    "h": args.h,
                    "k": args.k
                } for surrogate in [channel_signal]*args.num_surrogates
            ]
            pe_channel_signal = algos.nonlinear_prediction_error(*simulation_args.values(),
                                                                 sliding_window=args.sliding_window,
                                                                 overlap=0.5)
            pe_surrogates_channel_signal = [
                algos.nonlinear_prediction_error(*simulation_args_surrogates.values(),
                                                 sliding_window=args.sliding_window,
                                                 overlap=0.5,
                                                 surrogate=True)
                for simulation_args_surrogates in tqdm(simulation_args_surrogates_list)
            ]
            simulation_args.pop("signal")
            results = [
                "Results for %s, channel %s" % (args.eeg, channel),
                "Args: %s" % (repr(simulation_args),),
                "PE original signal: %s" % (repr(pe_channel_signal),),
                "PE surrogates: %s" % (repr(pe_surrogates_channel_signal),),
                "\n",
            ]
            results_str = "\n".join(results)

            fd.write(results_str)
            fd.flush()


if __name__ == '__main__':
    main()
