---
title: "Advanced Biosignal Analysis: Analysis of an EEG using nonlinear signal analysis with surrogates"
date: March 12, 2018
author:
  - Octavi Font Sola
  - Sira Mogas Díez
  - Celia Penella Hernández
geometry: margin=2.5cm
output: pdf_document
---

# 1. Introduction

Epilepsy is the fourth most common neurological disorder and can affect people of all ages (1). It is a brain disorder characterized by recurrent and unpredictable interruptions of the normal brain function, i.e. epileptic seizures, which can be defined as abnormal excessive or synchronous neuronal activity in the brain. This predisposition to generate epileptic seizures can lead to neurological, cognitive, psychological and social consequences (2).

More than 30 percent of patients with epilepsy have inadequate control of seizures with drug therapy (3), hence, there are different approaches for these patients. The most common diagnostic procedures imply the implantation of electrodes in the brain in order to help finding the epileptogenic focus (4). When doing this intervention, it is crucial to have an appropriate and accurate interpretation of the EEG recording to define the following steps.
EEG is an important tool for neurologists in the diagnosis and classification of seizures. It is not uncommon in clinical practice to see patients who were erroneously diagnosed as epileptic (5). For this reason, nowadays many interesting EEG classification procedures have been explored, trying to minimize the computational and training time.

One of the problems in areas of signal processing is to determine whether an observed time series is purely deterministic, contains a deterministic component, or is completely stochastic (6). In this project, we are exploring the possibility of obtaining a reliable method to predict signal dynamics, i.e. deterministic or stochastic behavior, by using simple quadratic procedures, which can be coupled with the implementation of surrogates. Surrogates are generated from an iterative randomized process which involves the calculation of the Fourier transformation of the original signal. As a result, different surrogates are obtained, all sharing same properties with the actual signal and with the peculiarity of serving as a reference probability distribution for a predefined null hypothesis. This null hypothesis considers that the analyzed signal is measured from an uncorrelated linear stochastic, stationary process. The comparison between the calculated nonlinear prediction error of the original signal and the one obtained for the generated surrogates allow us to determine whether the null hypothesis should be accepted or rejected. This can be a good indicator to understand signal dynamics.

The main aim is to be able to detect or recognize different signal dynamics patterns through the different parts of the EEG, i.e. differences between the pre-seizure, seizure and post-seizure periods, and differences in areas with the presence of artifacts. As it has already been explained, epilepsy implies hypersynchronous brain activity, but seizures can have different severities. Neurons are interconnected among them and thus, it is though that the brain areas involved in the epileptic processes share interdependencies, which can be reflected as changes in the dynamical patterns of the signals (7).  This could be an indicator of the seizure origin and therefore, this technique could be a first approximation to determine the location of the epileptic focus using a non-invasive approach.


# 2. EEG assessment
The first step consisted on the EEG visualization and its further interpretation. 
The given EEG, i.e. EEG 2, had 59 channels located in different parts of the right and left hemisphere of the brain, each of them recording brain activity. All of them were displayed with a certain separation between them for its analysis.

## 2.1 Seizure identification
In EEG 2, three main areas can be identified as shown in Fig. \ref{eeg_preictal}, \ref{eeg_ictal} and \ref{eeg_postictal}, being the period before the seizure starts (Fig. \ref{eeg_preictal}), the period during the seizure (Fig. \ref{eeg_ictal}) and the period after the seizure finishes (Fig. \ref{eeg_postictal}). There are many types of seizures, going from those which can easily be neglected or others which can cause severe complications, being able to cause major and irreversible brain injuries. The seizure implies the firing of neurons and can be identified by high intensities, the amplitude of the signal increases greatly (Fig. \ref{eeg_ictal}).

![EEG recorded during the preictal phase \label{eeg_preictal}](eeg_preictal.jpg){width=75%}

![EEG recorded during the ictal phase \label{eeg_ictal}](eeg_ictal.jpg){width=75%}

![EEG recorded during the postictal phase \label{eeg_postictal}](eeg_postictal.jpg){width=75%}

## 2.2 Artifacts
![EEG artifacts. All channels are represented in the y-axis over time, which is plotted in the x-axis and the artifacts are highlighted in different colours \label{eeg_artifacts}](eeg_artifacts.png){width=75%}

There are different types of artifacts which can be classified as mechanical or physiological. The mechanical ones mostly imply some kind of misconfiguration or movement of the electrodes and these tend to be short and usually affect few channels. However, physiological artifacts can come from the brain itself or from the different dipoles present in the body, such as the eyes and the heart.

In EEG 2, the main existing artifacts are mechanical and can mostly be detected after the seizure. These can be located between the second 280 and 300 and from the second 320 to the 350 approximately as seen in Fig. \ref{eeg_artifacts}. The different artifacts are highlighted in different colours. The artifact in blue correspond to a movement of the patient or its surrounding, causing this wavy pattern. The one highlighted in brown represents switch of some electrodes and it is visualized as a sudden noisy signal which can affect more than one channel. On the other hand, the green artifact might correspond to a strong movement of one of the electrode channels, which leads to a wide spikes. In this case, it only affects one channel, i.e. HSR5 channel.


# 3. Methodology

To perform the analysis over the EEG recording first we will implement the algorithm of the nonlinear prediction error (8)(9) and then run it over all the EEG channels and its surrogates. The prediction error of the surrogates acts as a null hypothesis for the original signal, and it can help us discriminate stochastic and purely deterministic signals and deterministic dynamics with noise, which can be used to identify the spatial origin of epileptic seizures.

The first step to compute the nonlinear prediction error is to reconstruct the phase space of the signal. We can't exactly reconstruct it from the given data but, by embedding the delayed signal by an amount $\tau$ a certain amount of times ($m$), we can obtain an approximation of the trajectories that would appear in the real phase space.

![Signal embedding and distance matrix functions \label{code_helper_fn}](code_helper_fn.png)

The next step of the algorithm is the calculation of the distance matrix (\_distance_matrix function in Fig. \ref{code_helper_fn}). It is a squared matrix which contains the Euclidean distances between the elements of the data set, and in this case, between the different time-series signals. It has the property of having all zeros, or in our case, all infinite values, on the main diagonal and it is symmetric, meaning that there is no need to compute all the distances of the given signal. Even though the distance matrix is not calculated with the values of the real phase space, the embedding has the property of keeping preserving the relative distances between points, which is key in the next steps, where we choose candidates based on their proximity.

![Nonlinear prediction error function \label{code_pe_fn}](code_pe_fn.png)

The last step was to calculate the nonlinear prediction error after normalizing the signal. Once the nonlinear prediction code was implemented (\_nonlinear_prediction_error function in Fig. \ref{code_pe_fn}), different approximations had to be tested as the nonlinear prediction error depends on its parameters, i.e. time delay, embedding dimensions, the number of k nearest neighbours, the prediction horizon and the Theiler correction. To determine the optimal parameters to analyze the given signals, different simulations had to be run in order to compare and pick the most accurate one.

![Prediction error is lower for nonlinear signals. As $\tau$ increases, so does PE \label{pe_test_signals}](pe_test_signals.jpg){width=75%}

Additionally, the accuracy of the algorithm was tested comparing it with provided results for five specific signals, results can be visualized in Fig. \ref{pe_test_signals}.

![Functions for the sliding window and the calculation of the surrogates \label{code_sliding_window_and_surrogates}](code_sliding_window_and_surrogates.png)

After having the nonlinear prediction error, the surrogates implementation was required (phase_randomized_surrogate in Fig. \ref{code_sliding_window_and_surrogates}). The number of surrogates q was established for a statistical significance of 0.1, resulting in a total number of 9 surrogates since $\alpha = \frac{1}{1 + q}$.

Since the spatial and computational cost of calculation for the whole signal at once would be too high and because the frequency spectrum is not equally distributed over time, it was necessary to compute both the surrogates and prediction error over the EEG using a sliding window. The sliding window slices the signal in chunks with a certain size and overlap we pass as parameters. Additionally, to speed up the execution of the simulations, we also parallelized the computation of the prediction error for the different sliding windows, which allowed us to run these experiments for different parameters in a reasonable amount of time.
The final parameters we used to obtain our results are as follows:


| parameter | value |
|----------:|:------|
| embedding dimension $m$ | 5 |
| delay time $\tau$ | 6 |
| Theiler correction $w$ | 25 |
| Horizon window $h$ | 65 |
| Nearest neighbours $k$ | 5 |
| Sliding window | 4096 |
| Overlap | 0.5 |
| num of surrogates | 9 |


# 4. Influence of the parameters
 
As mentioned before, one key aspect when calculating and interpreting the results of the nonlinear prediction error and surrogates is to make sure that all parameters have been correctly selected. There are some already defined ranges which are considered reasonable when implementing these algorithms but it is important to make sure that these ranges also work well for your given signal.

There are some aspects to take into consideration, which are the length of the signal and the interconnection between all parameters, as these do not work independently.

The simplest way to determine whether the parameters are correctly adjusted is to perform different tests in a known signal, i.e. white noise, and analyse which of the parameter combinations is giving a better approximation of the expected results for the signal.

Moreover, when implementing the surrogates for the EEG inspection, it is important to always use the same parameters to obtain uniform and comparable results.

In our case, be obtained most of the parameters values from the literature (10) and performed our exploratory analysis varying the delay time $\tau$ for the range [2, 6, 10, 14, 18, 22].

![Surrogate corrected prediction error grouped by hemisphere. Each subplot represents the results obtained by a different value of $\tau$ \label{LR_hemispheres_pe_all_simulations}](LR_hemispheres_pe_all_simulations.png){width=75%}

# 5. Results

Out of all the multiple simulations we performed, we ended up choosing the one with $\tau = 6$. As we saw in Fig. \ref{LR_hemispheres_pe_all_simulations} the trends of the surrogate corrected prediction error were stable across the experiments, so we chose a $\tau$ which best enhanced those tendencies.

Instead of using the raw data of the PE over time, which was difficult to interpret, we divided the signal in 3 periods: preictal, ictal and postictal. For each of those periods we calculated the mean of the ScPE (surrogate corrected prediction error) across its length. We also grouped the channels by hemisphere and area (we used the naming of the electrodes). With the first grouping we expected to spot the epileptic foci of the brain, while with the second we wanted to attempt a more precise localization.

As we can see in Fig. \ref{PE_LR_STSA_vs_NTSA}, there is a clear difference in the STSA analysis between hemispheres. This same graph, which has the same data with without the surrogate correction also emphasizes its importance. Without it, there are no clear diferences in the dynamics of the two hemispheres, whereas after the correction we can clearly appreciate two main patters. First, it is the different behaviour between EEG phases. The post ictal phase, on which the brain has return to normal function, the ScPE values are very low, more or less equally, across all hemispheres. During the ictal phase those values quadruple, but they are still on par between hemispheres. The high values can be explained due to the seemingly nonlinear nature of seizures, while the fact that the PE levels match across hemispheres would be caused by the propagation of the seizure along the whole of the brain. It is the preictal phase which gives us some insights regarding the lateralization of the seizure. The right hemisphere is showing a sixfold increase of ScPE compared to its left hemisphere, which would indicate a strong likelihood that the seizure originates from that region of the brain.

![ScPE vs PE for $\tau = 6$. Value averaged over the whole scan, preictal, ictal and post ictal periods. Channels grouped by brain hemisphere \label{PE_LR_STSA_vs_NTSA}](PE_LR_STSA_vs_NTSA.png){width=75%}

Since we wanted to be more specific regarding the localization of the epileptic focus, we also performed the same analysis as before but, instead of grouping channels by hemisphere, we did so by region (using the electrode names) as can be seen in Fig. \ref{ScPE_channels}. Our EEG was particularly good for this kind of analysis since the placement of the electrodes is symmetrical. From the results the activity on the TPR channels is particularly noticeable, doubling the next highest value (HAR channel). Since we wanted to discard that those results are being skewed by the presence of artifacts, we visually inspected the EEG for the TPR channels (see Fig. \ref{TPL_TPR_TBPL_TBPR_ALL}). As can be observed, on the preictal phase (before the 190s mark) TPR channels show a much higher level of activity compared to TPL. This activity, especially on TPR-4, resembles a Long Term Energy burst, as described in (11). These LTE bursts appear in increasing numbers in the numbers before a seizure develops and, from what we can see by the ScPE analysis, they have nonlinear properties.

To confirm the previous analysis and our suspicions (of the seizure originating on the right hemisphere) we also localized its onset. Surprisingly, the seizure onset is on the left hemisphere (TBPL-5, around the 195s mark). This contradicting information questions the predictive accuracy of the STSA method, but it could be explained if the origin of the seizure is located near pathways allowing rapid conduction of activity to the left hippocampus under some circumstances (11).

![ScPE for $\tau = 6$. Value averaged over the whole scan, preictal, ictal and post ictal periods. Channels grouped by name \label{ScPE_channels}](ScPE_channels.png){width=75%}

![EEG recording for channel groups TPL, TPR, TBPL and TBPR. Long Term Energy bursts are present in the TPR group (especially TPR-4). TBPL-5 has the onset of the seizure around the 195s mark \label{TPL_TPR_TBPL_TBPR_ALL}](TPL_TPR_TBPL_TBPR_ALL.png){width=75%}


# 6. Conclusions

The main aim of this practice was to find a way to inspect and distinguish different features in EEG recordings in an automatic manner to reduce the time spent interpreting the results and to find a non-invasive and effective way to identify the epileptic focus based on the signal dynamics.

We would also like to comment on the feasibility of performing this kind of analysis. Even though it is a computationally intensive algorithm (moreso since multiple surrogates need to be computed), it is also easy to parallellize and scale. Our implementation is already capable of multiprocessing, and it can perform multiple experiments in parallel as well, which should speed up the choice of hyper parameters (another of the drawbacks). This makes it a cluster-friendly technique for this new age of multicore computing.

Applying surrogate corrected nonlinear time analysis techniques we've been able to better understand the neuronal dynamics on different phases of an EEG. ScPE ability to characterize those dynamics is especially useful in the preictal stage, which provides a set of practical tools, albeit inconclusive, to localize the origin of seizures, which can be employed as a diagnostic tool for clinicians, particularly in cases where findings by other traditional methods happen to be inconsistent. Even though with this analysis we tried to go further than a simple lateralization of the focus of the seizure, the results do not match the onset of the EEG, so being more specific would be adventurous. Similar analyses should be performed on more EEGs before leaping to conclusions.


# 7. Bibliography

1. What is epilepsy? [Internet]. Landover: Epilepsy Foundation [citation date 08/03/2018]. Avaliable at: https://www.epilepsy.com/learn/about-epilepsy-basics/what-epilepsy
2. Fisher, R. S., Boas, W. E., Blume, W., Elger, C., Genton, P., Lee, P., Engel J. Epileptic Seizures and Epilepsy: Definitions Proposed by the International League Against Epilepsy (ILAE) and the International Bureau for Epilepsy (IBE). ILAE. March 2005; 46 (4): 470-472.
3. Kwan, P., Brodie, M. J. Early Identification of Refractory Epilepsy. N Engl J Med. February 2000; 342 (5): 314-319.
4. Vakharia, V. N., Sparks, R., O’Keeffe, A. G., Rodionov R, Miserocchi A, McEvoy A, Ourselin S, Duncan J. Accuracy of intracranial electrode placement for stereoelectroencephalography: A systematic review and meta-analysis. ILAE. March 2017; 58 (6): 921-932.
5. Dericioglu, N., Ozdemir, P. The Success Rate of Neurology Residents in EEG Interpretation After Formal Training. Clin EEG Neurosci. October 2017; 49 (2): 136-140.
6. Small, M., Tse, C. K. Detecting Determinism in Time Series: The Method of Surrogate Data. IEEE. May 2003; 50 (5): 663-672.
7. R. G. Andrzejak, D. Chicharro, K. Lehnertz, and F. Mormann. Using bivariate signal analysis to characterize the epileptic focus: The benefit of surrogates. Phys. Rev. April 2011; 83 (4): 046203.
8. Kantz, H. and Schreiber, T. (1997) Nonlinear Time Series Analysis. Cambridge University Press, Cambridge.
9. Andrzejak, R. G., Lehnertz, K., Mormann, F., Rieke, C., David, P., & Elger, C. E. (2001). Indications of nonlinear deterministic and finite-dimensional structures in time series of brain electrical activity: Dependence on recording region and brain state. Physical Review E, 64(6), 061907.
10. Andrzejak, R. G., Mormann, F., Widman, G., Kreuz, T., Elger, C. E., Lehnertz, K. Improved spatial characterization of the epileptic brain by focusing on nonlinearity. J Epilepsy Res. February 2006; 69(1): 30-44.
11. Litt, B., Esteller, R., Echauz, J., D’Alessandro, M., Shor, R., Henry, T., … Vachtsevanos, G. (2001). Epileptic seizures may begin hours in advance of clinical onset: A report of five patients. Neuron, 30(1), 51–64. https://doi.org/10.1016/S0896-6273(01)00262-8

