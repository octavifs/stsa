\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[a4paperpaper,twocolumn]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Advanced Biosignal Analysis: Analysis of an EEG using nonlinear signal analysis with surrogates},
            pdfauthor={Octavi Font Sola; Sira Mogas Diez; Celia Penella Hernandez},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Advanced Biosignal Analysis: Analysis of an EEG using nonlinear signal
analysis with surrogates}
\author{Octavi Font Sola \and Sira Mogas Diez \and Celia Penella Hernandez}
\date{12 March 2018}

\begin{document}
\maketitle

\hypertarget{introduction}{%
\section{1. Introduction}\label{introduction}}

Epilepsy is the fourth most common neurological disorder and can affect
people of all ages (1). It is a brain disorder characterized by
recurrent and unpredictable interruptions of the normal brain function,
i.e.~epileptic seizures, which can be defined as abnormal excessive or
synchronous neuronal activity in the brain. This predisposition to
generate epileptic seizures can lead to neurological, cognitive,
psychological and social consequences (2).

More than 30 percent of patients with epilepsy have inadequate control
of seizures with drug therapy (3), hence, there are different approaches
for these patients. The most common diagnostic procedures implies the
implantation of electrodes in the brain in order to help finding the
epileptogenic focus (4). When doing this intervention, it is crucial to
have an appropriate and accurate interpretation of the EEG recording to
define the following steps.

EEG is an important tool for neurologists in the diagnosis and
classification of seizures. It is not uncommon in clinical practice to
see patients who were erroneously diagnosed as epileptic (5). For this
reason, nowadays many interesting EEG classification procedures have
been explored, trying to minimize the computational and training time.

One of the problems in areas of signal processing is to determine
whether an observed time series is purely deterministic, contains a
deterministic component, or is completely stochastic (6). In this
project, we are exploring the possibility of obtaining a reliable method
to predict signal dynamics, i.e.~deterministic or stochastic behaviour,
by using simple quadratic procedures, which can be coupled with the
implementation of surrogates. Surrogates are generated from an iterative
randomized process which involves the calculation of the Fourier
transformation of the original signal. As a result, different surrogates
are obtained, all sharing same properties with the actual signal and
with the peculiarity of serving as a reference probability distribution
for a predefined null hypothesis. This null hypothesis considers that
the analysed signal is measured from an uncorrelated linear stochastic,
stationary process. The comparison between the calculated nonlinear
prediction error of the original signal and the one obtained for the
generated surrogates allow us to determine whether the null hypothesis
should be accepted or rejected. This can be a good indicator to
understand signal dynamics.

The main aim is to be able to detect or recognize different signal
dynamics patterns through the different parts of the EEG,
i.e.~differences between the pre-seizure, seizure and post-seizure
periods, and also differences in areas with the presence of artifacts.
As it has already been explained, epilepsy implies hypersynchronous
brain activity but seizures can have different severities. Neurons are
interconnected among them and thus, it is though that the brain areas
involved in the epileptic processes share interdependencies, which can
be reflected as changes in the dynamical patterns of the signals (7).
This could be an indicator of the seizure origin and therefore, this
technique could be a first approximation to determine the location of
the epileptic focus using a non-invasive approach.

\hypertarget{eeg-assessment}{%
\section{2. EEG assessment}\label{eeg-assessment}}

The first step consisted on the EEG visualization and its further
interpretation. The given EEG, i.e.~EEG 2, had 61 channels located in
different parts of the right and left hemisphere of the brain, each of
them recording brain activity. All of them were displayed with a certain
separation between them for its analysis.

\hypertarget{seizure-identification}{%
\subsection{2.1 Seizure identification}\label{seizure-identification}}

In EEG 2, three main areas can be identified as shown in Fig. 1, being
the period before the seizure starts (Fig. 1A), the period during the
seizure (Fig. 1B) and the period after the seizure finishes (Fig. 1C).
There are many types of seizures, going from those which can easily be
neglected or others which can cause severe complications, being able to
cause major and irreversible brain injuries. In this case, the seizure
is quite severe as the firing of neurons has a really high intensity,
the amplitude of the signal increases greatly (Fig. 1B).

\hypertarget{artifacts}{%
\subsection{2.2 Artifacts}\label{artifacts}}

There are different types of artifacts which can be classified as
mechanical or physiological. The mechanical mostly imply some kind of
misconfiguration or movement of the electrodes and these tend to be
short and usually affect few channels. However, physiological artifacts
can come from the brain itself or from the different dipoles present in
the body, such as the eyes and the heart.

In EEG 2, the main existing artifacts are mechanical and can mostly be
detected after the seizure. They can be located between the 280 and 300
seconds and from 320 to 410 seconds approximately as seen in Fig. 2A.
The different artifacts are highlighted in different colours. The green,
purple and blue ones correspond to movement of the electrodes, causing
these wavy pattern. On the other hand, the red artifact might correspond
to a sudden switch of the HSR5 channel, causing that pick. Finally, in
the TPL4, channel which is highlighted in yellow (Fig. 2B), a periodic
pattern can be observed, being similar to cardiac action potentials.
Thus, it could be hypothesised that this artifact could come from a
blood vessel in the brain close to the particular electrode.

\#~3. Methodology As stated in the introduction, the objective of this
study is to be able to differentiate EEG signal dynamics patterns
through the different parts of the EEG. The methodology consisted of
applying a combination of nonlinear prediction error and surrogates.
This approach can help us discriminate stochastic and purely
deterministic signals and deterministic dynamics with noise, which can
be used to identify the spatial origin of epileptic seizures.

An algorithm was developed which combined the computation of the
nonlinear prediction error of the original signal, the calculation of
the surrogates of this signal and the computation of the nonlinear
prediction error of the surrogates obtained.

\begin{verbatim}
def _delayed_signal(signal, m, tau):
    signal_length=signal.shape[0]
    delayed=np.zeros((m,signal_length-(m-1)*tau), dtype=signal.dtype)
    for idx in range(m):
        start = (m-1-idx) * tau
        end = signal_length - idx * tau
        delayed[idx, :] = signal[start:end]
    return delayed

def _euclidean(v1, v2):
    return np.sqrt(np.sum((v1 - v2)**2, axis=0))

def _distance_matrix(signal,w,h):
    rows, columns = signal.shape
    distance = np.ones((columns, columns), dtype=signal.dtype) * np.inf
    for j in range(columns-h):
        i_idxs = np.array(range(j+1+w, columns-h))
        if not len(i_idxs):
            continue
        distances = _euclidean(signal[:, i_idxs], signal[:, j][:, np.newaxis])
        distance[i_idxs, j] = distances
        distance[j, i_idxs] = distance[i_idxs, j]
    return distance
\end{verbatim}

The first step is to compute the nonlinear prediction error is to
estimate the trajectory of the signal since it is not possible to
reconstruct its complete trajectory in the original state space. An
estimation can be obtained by delaying the signal for for a certain
\(\tau\) within a specific embedding window m (\_delayed signal function
in Figure 3).

The next step of the algorithm is the calculation of the distance matrix
(\_distance matrix function in Figure 3). It is a squared matrix (2D
array) which contains the Euclidean distances between the elements of
the data set, and in this particular case, between the different
time-series signals. It has the property of having all zeros, or in our
case, all infinite values, on the main diagonal and it is symmetric,
meaning that there is no need to compute all the distances of the given
signal.

\begin{verbatim}
def _nonlinear_prediction_error(signal, m, tau, w, h, k):
    """ Calculates the nonlinear prediction error for a given signal
    :param signal: 1D np.array
    :param m: embedding dimension
    :param tau: delay time
    :param w: Theiler correction window
    :param h: horizon window
    :param k: nearest neighbours
    :return: float with the nonlinear prediction error for the given signal and params
    """
    signal=(signal-signal.mean())/signal.std()
    delayed=_delayed_signal(signal, m, tau)
    dist = _distance_matrix(delayed,w,h)
    [error, error_arr]=_prediction_error(signal, dist, m, tau, h, k)
    return error

def _nonlinear_prediction_error_unordered(args):
    result = _nonlinear_prediction_error(*args)
    return args, result
\end{verbatim}

The last step was to calculate the non-linear prediction error after
normalizing the signal with the standard deviation. Once the nonlinear
prediction code was implemented (\_nonlinear\_prediction\_error function
in Figure 5), different approximations had to be tested as the nonlinear
prediction error depends on its parameters, i.e.~time delay, embedding
dimensions, the number of k nearest neighbours, the prediction horizon
and the Theiler correction. To determine the optimal parameters to
analyse the given signals, different simulations had to be run in order
to compare and pick the most accurate one.\\
Additionaly, the accuracy of the algorithm was tested comparing it with
provided results for five specific signals, results can be visualized in
Fig. 6.

To optimize the computational cost of the algorithm, a sliding window
was implemented (nonlinear\_prediction\_error in Figure 7). The sliding
window was necessary due to the size of the signal to be analyzed. EEG
recordings have long durations, which result in time series of millions
of data points. Therefore, the sliding window was required in order to
be able to analyze the whole signal, not separate fragments, which could
lead to misleading results since only a fraction of the power spectrum
is assessed. Together with this implementation, a multiprocessing
approach was included for the different sliding windows to parallelize
the computation of each process.

\begin{verbatim}
def nonlinear_prediction_error(signal, m, tau, w, h, k, \
        sliding_window=None, overlap=0.5, processes=os.cpu_count()):
    """ Calculates the nonlinear prediction error for a given signal
    :param signal: 1D np.array
    :param m: embedding dimension or list of same
    :param tau: delay time or list of same
    :param w: Theiler correction window or list of same
    :param h: horizon window or list of same
    :param k: nearest neighbours or list of same
    :param sliding_window: If specified, calculate the nonlinear
    prediction error as the mean of the sliding window.
    :param overlap: float that specified how much of an overlap between
    sliding windows there should be. Between 0 and 1
    :return: dict with float with the nonlinear prediction error for the
    given signal and params or just a float if there is only 1 combination
    of params
    """
    if not isinstance(m, Iterable):
        m = [m]
    if not isinstance(tau, Iterable):
        tau = [tau]
    if not isinstance(w, Iterable):
        w = [w]
    if not isinstance(h, Iterable):
        h = [h]
    if not isinstance(k, Iterable):
        k = [k]

    if not sliding_window:
        sliding_window = len(signal)

    offset = int(sliding_window * overlap)
    num_cuts = int((len(signal) - offset) / offset)
    signal_cuts = [signal[idx:idx+sliding_window] for idx in range\
                   (0, num_cuts*offset, offset)]
    multiprocessing_args = list(product(signal_cuts, m, tau, w, h, k))
    multiprocessing_results = {args[1:]: [] for args in multiprocessing_args}
    multiprocessing_aggregated_results = {}

    with multiprocessing.Pool(processes=processes) as pool:
        with tqdm(total=len(multiprocessing_args)) as pbar:
            for args, result in pool.imap_unordered(
                _nonlinear_prediction_error_unordered, multiprocessing_args
            ):
                pbar.update()
                multiprocessing_results[args[1:]].append(result)
        for key, values in multiprocessing_results.items():
            multiprocessing_aggregated_results[key] = np.array(values).mean()

    if len(multiprocessing_aggregated_results) == 1:
        return list(multiprocessing_aggregated_results.values())[0]
    else:
        return multiprocessing_aggregated_results
\end{verbatim}

\begin{figure*}
\centering
\includegraphics{si_pe-vs-su_pe.png}
\caption{Prediction error of the signal vs its surrogates over time}
\end{figure*}

\end{document}
