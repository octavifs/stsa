#!/bin/sh

# Calculating PE for the following channels and intervals
# for all channels on the following intervals
#
# pre-ictal
# 0 90000
#
# ictal
# 1090000 1240000
#
# post-ictal
# 1410000 2290000
#
# for taus = [20, 32]
#

# pre-ictal
python compute_pe.py EEG2.mat 0 90000 results/ALL_t20_preictal.out --tau=6
python compute_pe.py EEG2.mat 0 90000 results/ALL_t32_preictal.out --tau=6

# ictal
python compute_pe.py EEG2.mat 109000 124000 results/ALL_t20_ictal.out --tau=6
python compute_pe.py EEG2.mat 109000 124000 results/ALL_t32_ictal.out --tau=6

# post-ictal
python compute_pe.py EEG2.mat 141000 229000 results/ALL_t20_postictal.out --tau=6
python compute_pe.py EEG2.mat 141000 229000 results/ALL_t32_postictal.out --tau=6
